from PIL import Image
from urllib.parse import urlencode
from bs4 import BeautifulSoup
from io import BytesIO

import requests
import math
import re


class EbayService:
    headers = {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36"
    }

    def __init__(self, username):
        self.username = username

    def pages(self, tree):
        """
        -> Checks nb of items found and nb of pages to process (using a layout of 200 items/page)
        """
        nb_item = tree.find("span", {"class": "rcnt"})
        nb_item = nb_item.text
        nb_pages = math.ceil(int(nb_item) / 200)
        return nb_pages

    def ebay_url(self, page):
        return 'https://www.ebay.com/sch/m.html?' + urlencode({'_ssn': self.username, '_pgn': page, 'LH_BIN': 1, '_ipg': 200, '_sacat': 11450})

    def product_links(self):
        result = []

        page_link = self.ebay_url(1)

        page_response = requests.get(page_link, headers=self.headers).text
        soup = BeautifulSoup(page_response, "html.parser")

        nb_pages = self.pages(soup)

        if int(nb_pages) == 0:
            print("0 pages found")
        else:
            x = 1
            while x <= nb_pages:
                page_link = self.ebay_url(x)
                print(page_link)
                page_response = requests.get(
                    page_link, headers=self.headers).text
                soup = BeautifulSoup(page_response, "html.parser")
                search_results = soup.findAll("li", {"class": "sresult"})
                for item in search_results:
                    result.append(
                        str(item.find("a", {"class": "vip"}).attrs['href']))
                x += 1
            else:
                print("[+]Done. No more pages to process.")
            return result

    def ebay_pillow(self, img_link):
        response = requests.get(img_link)
        img = Image.open(BytesIO(response.content))
        img = img.convert('RGB')
        img = img.resize((1280, 1280))
        buffer = BytesIO()
        img.save(buffer, 'JPEG')
        buffer.seek(0)
        return buffer

    def scrape_product(self, url):
        product = {}

        page_response = requests.get(url, headers=self.headers).text
        soup = BeautifulSoup(page_response, "html.parser")

        # product link
        product['product_link'] = f"{url}&nordt=true"

        # title
        title = soup.find("h1", {"id": "itemTitle"})
        if title is not None:
            product['title'] = title.text.strip('Details about')

        # notes
        notes = soup.find("span", {"class": "viSNotesCnt"})
        if notes is not None:
            product['notes'] = "Notes: " + notes.text

        # price
        price = soup.find("span", {"id": "mm-saleDscPrc"}
                          ) or soup.find("span", {"id": "prcIsum"})
        if price is not None:
            product['price'] = float(price.text.strip().lstrip('US $'))

        # location
        location = soup.find("span", {"itemprop": "availableAtOrFrom"})
        if location is not None:
            product['location'] = location.text
        else:
            product['location'] = None

        # shipping price
        shipping_price = soup.find("span", {"id": "fshippingCost"})
        if shipping_price is not None:
            if shipping_price.text.strip() == "FREE":
                product['shipping_price'] = 0.00
            else:
                product['shipping_price'] = float(
                    shipping_price.text.strip().lstrip('$'))
        else:
            product['shipping_price'] = None

        # images
        product_images = []
        mainImgDiv = soup.find("div", {"id": "vi_main_img_fs_slider"})
        if mainImgDiv is not None:
            for anchr in mainImgDiv.find_all("a", {"class": "pic pic1"}):
                for img in anchr.find_all("img"):
                    product_image = img.attrs['src'].replace(
                        "s-l64", "s-l1600")
                    product_images.append(product_image)

        product['ebay_images'] = product_images[:4]

        # category
        bcrumbs = soup.find("td", {"id": "vi-VR-brumb-lnkLst"})
        category = bcrumbs.find("a", {"class": "scnd"})

        product['sacat'] = int(
            re.search(r'([\d]+)', category.attrs['href']).group())

        # brand
        brand = soup.find("h2", {"itemprop": "brand"})
        if brand is not None:
            product['brand'] = brand.text

        # total price
        if product['shipping_price'] is not None:
            total = product['price'] + product['shipping_price']
        else:
            total = product['price']

        product['total_price'] = total

        # size
        count = 0
        found = 0
        table = soup.findAll("div", {"class": "itemAttr"})
        field_name = table[0].findAll("td", {"class": "attrLabels"})
        value = table[0].findAll("td", {"width": "50.0%"})
        for field in field_name:
            name = field.text.strip()
            if name == 'Size:' or name == 'Size (Men\'s):' or name == 'Size (Women\'s):' or name == 'US Shoe Size (Men\'s):' or name == 'US Shoe Size (Women\'s):' or name == 'Bottoms Size (Men\'s):' or name == 'Bottoms Size (Women\'s):':
                found = 1
                break
            count += 1
        if found == 0:
            title_list = product['title'].split()
            size_list = ["Size", "size", "Size:",
                         "size:", "Sz", "sz", "Sz:", "sz:"]
            for i in range(len(title_list)):
                if title_list[i] in size_list:
                    size_value = title_list[i + 1]
                    break
                else:
                    size_value = "NA"
        else:
            size_value = value[count].text.strip()

        product['size'] = size_value

        # description
        if notes is not None:
            product['description'] = f"{product['title']}\n {product['notes']}"
        else:
            product['description'] = f"{product['title']}"
        return product
