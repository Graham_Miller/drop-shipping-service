class Product:
    def __init__(self, description, images, category, quantity, price, videos=None, title=None, brand=None, size=None):
        self.description = description
        self.images = images
        self.videos = videos
        self.category = category
        self.quantity = quantity
        self.price = price
        self.title = title
        self.brand = brand
        self.size = size
