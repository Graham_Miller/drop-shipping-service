from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from botocore.client import Config
from pymongo import MongoClient
from urllib.parse import urlencode
from bs4 import BeautifulSoup
from io import BytesIO
from product import Product
from ebay_service import EbayService

import uuid
import requests
import json
import random
import time
import re
import threading
import boto3
import math


MONGO_CLIENT = MongoClient(
    'mongodb+srv://admin:M1kPMUqn4M2xDEuf@depop-users-db-wqwyq.mongodb.net/test?retryWrites=true')

DPOP_USERS_DB = MONGO_CLIENT['depop-users-db']
DPOP_LIKERS = DPOP_USERS_DB['likers']

def main():
    optionsPrompt = input(
        "Would you like to scrape posts from user and upload to depop `s`, check links from the user `c`, or exit `e`? ")

    if optionsPrompt == 's':
        ebay = EbayService("haze1230_2")
        links = ebay.product_links()
        products = []

        for link in links:
            products.append(ebay.scrape_product(link))
            print(f"{link} ({links.index(link) + 1}/{len(links)})")
        print(products)
    elif optionsPrompt == 'c':
        pass
    else:
        pass


if __name__ == '__main__':
    main()
