from botocore.client import Config

import boto3
import json
import requests
import uuid
import re
import random
import time
import app


class Depop:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.token = self.login()
        self.app_headers = {"User-Agent": "Depop Android X 2.41.1 rv:17089638 (SM-G955N - 5.1.1; * en_US)", "X-Garage-bundle-id": "com.depop", "X-Country-Code": "US", "Accept-Language": "en-US",
                            "Content-Type": "application/json", "Connection": "close", "Authorization": f"Bearer {self.token}", "Accept-Encoding": "gzip, deflate", "If-Modified-Since": "Sat, 16 Mar 2019 06:22:33 GMT"}
        self.user_info = self.account_info()

    def login(self):
        login_url = "https://webapi.depop.com:443/api/auth/v1/login"
        login_headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0", "Accept": "application/json", "Accept-Language": "en-US,en;q=0.5",
                         "Accept-Encoding": "gzip, deflate", "Referer": "https://www.depop.com/", "Content-Type": "application/json", "Origin": "https://www.depop.com", "Connection": "close"}
        login_json = {"password": self.password, "username": self.username}
        return requests.post(login_url, headers=login_headers, json=login_json).json()['token']

    def account_info(self):
        burp0_url = "https://api.depop.com:443/api/v1/users/me/"
        json_data = requests.get(burp0_url, headers=self.app_headers).json()

        return {"id": json_data['id'], "selling_count": json_data['selling_count']}

    def product_ids(self):
        burp0_url = f"https://api.depop.com:443/api/v1/users/{self.user_info['id']}/products/search/?offset_id=&limit={self.user_info['selling_count']}"
        json_data = requests.get(burp0_url, headers=self.app_headers).json()

        return [item['id'] for item in json_data['objects']]

    def notifications(self):
        burp0_url = f"https://api.depop.com:443/api/v2/notifications/me/?user_id={self.user_info['id']}&limit=20"

        return requests.get(burp0_url, headers=self.app_headers).json()

    def send_message(self, receiver_id, message):
        burp0_url = "https://api.depop.com:443/api/v2/chat/messages"
        burp0_json = {"idempotency_key": str(uuid.uuid1()),
                      "text": message, "user_id": receiver_id}

        requests.post(burp0_url, headers=self.app_headers, json=burp0_json)

    def get_product(self, product_id):
        burp0_url = f"https://api.depop.com:443/api/v1/products/{product_id}/"
        return requests.get(burp0_url, headers=self.app_headers).json()

    def upload_image(self, buffer):
        burp0_url = "https://api.depop.com:443/api/v1/pictures/"
        burp0_json = {"extension": "jpg", "picture_type": "P0"}
        r = requests.post(burp0_url, headers=self.app_headers, json=burp0_json)
        t = json.loads(r.content)

        ACCESS_KEY_ID = t['aws_credentials']['access_key_id']
        SESSION_TOKEN = t['aws_credentials']['session_token']
        ACCESS_SECRET_KEY = t['aws_credentials']['secret_access_key']
        BUCKET_NAME = t['aws_bucket']
        AWS_KEY = t['aws_key']
        RESOURCE_URI = t['resource_uri']

        s3 = boto3.resource(
            's3',
            aws_access_key_id=ACCESS_KEY_ID,
            aws_secret_access_key=ACCESS_SECRET_KEY,
            aws_session_token=SESSION_TOKEN,
            config=Config(signature_version='s3v4')
        )
        s3.Bucket(BUCKET_NAME).put_object(
            Key=AWS_KEY, Body=buffer)

        print("Done, here's your picture: https://" + BUCKET_NAME +
              ".s3.amazonaws.com/" + AWS_KEY)

        return RESOURCE_URI

    def upload_product(self, product: dict):
        new_product_url = "https://api.depop.com:443/api/v1/products/"
        if product['size']:
            size = product['size']['variants']
        else:
            size = product['size']

        new_product_json = {
            "address": product['location'],
            "brand_id": product['brand'],
            "categories": [product['category']],
            "description": product['description'],
            "hand_delivery": False,
            "international_shipping_cost": None,
            "national_shipping_cost": "0",
            "pictures": product['depop_images'],
            "place_data": {
                "address_components": [{
                    "long_name": "United States",
                    "short_name": "US",
                    "types": ["country", "political"]
                }],
                "formatted_address": "Mountain View, California, US", "geometry": {
                    "bounds": None,
                    "location": {"lat": 37.4232962, "lng": -122.0855478},
                    "location_type": None,
                    "viewport": None
                },
                "types": None
            },
            "price_amount": product['total_price'],
            "price_currency": "USD",
            "purchase_via_paypal": True,
            "quantity": product['quantity'],
            "selling_mode": "CLASSIC",
            "share_on_fb": False,
            "share_on_tw": False,
            "shippable": False,
            "shipping_methods": None,
            "variant_set": product['variant_set'],
            "variants": size,
            "video_ids": []
        }

        return requests.post(new_product_url, headers=self.app_headers, json=new_product_json).content

    def update_product(self, product: dict):
        burp1_url = f"https://api.depop.com:443/api/v1/products/{product['id']}/"

        if product['shipping_methods']:
            del product['shipping_methods'][0]['id']
            del product['shipping_methods'][0]['product_id']

        if 'variants' in product:
            keys = re.findall("\d+", str(product['variants'].keys())) # pylint: disable=anomalous-backslash-in-string
            for key in keys:
                product['variants'][key] = 0
            quantity = None
        else:
            quantity = int(product['quantity']) - 1
            product['variants'] = None

        burp1_json = {
            "brand_id": product['brand_id'],
            "categories": product['categories'],
            "description": str(product['description']),
            "hand_delivery": False,
            "international_shipping_cost": None,
            "national_shipping_cost": str(product['national_shipping_cost']),
            "pictures": [f"/api/v1/pictures/{item['id']}/" for item in product['pictures_data']],
            "price_amount": str(product['price_amount']),
            "price_currency": "USD",
            "purchase_via_paypal": True,
            "quantity": quantity,
            "selling_mode": "CLASSIC",
            "share_on_fb": False,
            "share_on_tw": False,
            "shippable": False,
            "shipping_methods": product['shipping_methods'],
            "variant_set": product['variant_set'],
            "variants": product['variants'],
            "video_ids": []
        }

        return requests.patch(burp1_url, headers=self.app_headers, json=burp1_json).content

    def delete_product(self, product_id):
        burp0_url = f"https://api.depop.com:443/api/v1/products/{product_id}/"
        requests.delete(burp0_url, headers=self.app_headers)

    def update_products(self):
        product_ids = self.product_ids()
        while True:
            for product_id in product_ids:
                print(f"{self.update_product(self.get_product(product_id))}\n")
            time.sleep(random.randint(3600, 3700))

    def auto_messenger(self):
        while True:
            notification_data = self.notifications()
            for i in range(len(notification_data['objects'])):
                username = notification_data['objects'][i]['object']['sender']['username']
                receiver_id = notification_data['objects'][i]['object']['sender']['id']

                if notification_data['objects'][i]['object']['type'] == 'like' and not app.DPOP_USERS_DB.find_one({'username': username}):
                    app.DPOP_LIKERS.insert_one(
                        {'username': username, 'id': receiver_id})
                    time.sleep(10)
                    self.send_message(
                        receiver_id, "hey you interested in a bundle deal?")
            time.sleep(random.randint(1800, 3700))
